# Dart code convention highlight

## TLDR;
- camelCase
- PascalCase
- snake_case
- kebab-case

## Naming:
Use camelCase for variable, function names: <br />
Ex:
```
int increase()
```
```
var count = 0;
```
Prefer to use private variable/function: 
```
var _count = 0; instead var count = 0;

_myFunction() instead myFunction() 
```
Use PascalCase for class name:
```
class MyClass {

}
```
Use snake_case or kebab-case for file/folder, snake_case is prefered: <br/>
Ex:
```
|-lib/src
|-lib/src/model/base_response.dart
```
[Read more from Effective Dart](https://dart.dev/guides/language/effective-dart/style)
## Project structure:
Main structure
```
|-android # native Android project
|-assets # resources
|-ios # native iOS project
|-lib # flutter source code
|-pubspec.yaml # Flutter packages and configuration
```
Flutter source code structure:
```
|-lib/...
|-lib/src/...
|-lib/src/common/ (a)
|-lib/src/model/ (b)
|-lib/src/network/ (c)
|-lib/src/repositories/ (d)
|-lib/src/screen/ (e)
|-lib/src/util/ (f)
|-lib/src/widgets/ (g)
|-lib/app.dart  (1)
|-lib/main.dart (2)
|-lib/routes.dart (3)
```
- (1) Declare MaterialApp and global configuration such as Bloc/Repository providers, localization, notification,... with Application context
- (2) Config before run MaterialApp such as Crashlytics, Bloc observer, Storage,....
- (3) Whole app routes, use with MaterialApp onGenerateRoute method

### (a) **common** items:
- Contains general items, should be reuse in others projects
```
|-base/ # Common widgets
|-bloc/ # Bloc related items
|-storage/ # Offline storage such as SharePreferences,...
|-translation/ # Custom app translation
|-app_bloc_observer.dart # Bloc observer
|-config.dart # Global config: app's name, url,...
|-extensions.dart # function extensions
|-notification_helper.dart # Push notification helper
|-resources.dart # Resource like file, image,.. path
|-style.dart # Global app's styling
|-ui_helper.dart # functions relate to UI: showPopup, 
showLoading,...
```
### (b) **model** : App's models. <br/>

**Do not include any UI function here** <br/>
Model can generate from json with this tool: https://app.quicktype.io/
![quicktype](images/quicktype.png)

### (c) **network**: Network clients <br/>
- http client: Dio https://pub.dev/packages/dio
- client generator with Retrofit: https://pub.dev/packages/retrofit <br/>
- main app's client:
```
rest_client.dart
```
### (d) **repositories**: App's repositories <br/>
- Data repository: Combine data client/providers. Where to interact with Apis, local storage,....
```
data_repository.dart
``` 
*DataRepository should be inject with Provider and pass to every Bloc/Cubit in constructor*
### (e) **screens**: App's screens
- Each screen should contains:
```
|{screen name}_screen.dart # ex: login_screen.dart
|cubit/{cubit name}_cubit.dart # ex: login_cubit.dart
|cubit/{state name}_state.dart # ex: login_state.dart
|bloc/... # (optional) same structure with Cubit
|widgets/... # (optional) custom widget, same structure with screen
```
### (f) **util**: Utilize functions
Do not include project specific functions and can be reuse
### (G) **widgets**: Custom widgets
Do not include project specific widgets and can be reuse
