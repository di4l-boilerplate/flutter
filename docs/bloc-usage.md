# Bloc Usage
- Use this library to implement BLOC architecture: https://bloclibrary.dev/
- Install Bloc plugin for Android studio: 
![bloc-plugin](images/bloc_plugin.png)
- Follow steps below to create new widget/screen to working with BaseCubit:
  ```
  1. Generate Cubit using Bloc's plugin
  2. Modify generated State extend BaseState
  3. Modify generated Cubit extend BaseCubit
  4. Create widget extends CubitWidget
  ```
  We're trying to create Register screen 
## 1. Generate Cubit/Bloc
- Create `register/cubit` folder
- Right click on `cubit` folder and select `Cubit Class` 
![generate-cubit](images/generate_cubit.png)
then enter `Register`
![enter-cubit-name](images/enter_cubit_name.png)
- This step will generate two files: `register_cubit.dart` and `register_state.dart`

## 2. Modify generated State extend BaseState
Current code look like:
```
part of 'register_cubit.dart';

@immutable
abstract class RegisterState {}

class RegisterInitial extends RegisterState {}
```
Modify `RegisterState` to extends `BaseState` <br/>
Add default constructor and named constructor `RegisterState.fromInternal` <br/>
This constructor is required for general handle cases <br/>
The code now look like:
```
class RegisterState extends BaseState {
  RegisterState();
  RegisterState.fromInternal(dynamic internalState, RegisterState state);
}
```
Add `fromInternal` constructor to `factories` file `common/factories_states.dart`
```
RegisterState: (dynamic internalState, RegisterState state) =>
      RegisterState.fromInternal(internalState, state)
```
`factories_states.dart` will look like:
```
final factories = <Type, Function>{
  LoginState: (dynamic internalState, LoginState state) =>
      LoginState.fromInternal(internalState, state),
  MainState: (dynamic internalState, MainState state) =>
      MainState.fromInternal(internalState, state),
  RegisterState: (dynamic internalState, RegisterState state) =>
      RegisterState.fromInternal(internalState, state)
};

T make<T extends BaseState>(dynamic internalState, T state) {
  return factories[T](internalState, state);
}
```
## 3. Modify generated Cubit extend BaseCubit
Current code `register_cubit.dart` look like:
```
class RegisterCubit extends Cubit<RegisterState> {
  RegisterCubit() : super(RegisterInitial());
}
```
Modify it to extends `BaseCubit` with `RegisterState`, after fixing overide issue, the code should look like:

![register_cubit_new](images/register_cubit_new.png)

after implementation:

![regiter_cubit](images/regiter_cubit.png)

## 4. Create widget extends CubitWidget
- Create `RegisterScreen` extends `CubitWidget<RegisterCubit, RegisterState>` in `register_screen.dart`
- Overide require methods, add Scaffold widget, the code should look like:
![register_screen](images/register_screen.png)

- Explain:
![cubit_widget_explain](images/cubit_widget_explain.png)
  
## Examples:
Load data and errors handler:
```
demoLoadData() async {
    emitLoading(true);
    await Future.delayed(Duration(seconds: 3));
    try {
      await _errorLoaded();
      // final response = await _successLoaded();
      // print(response);
      emitLoading(false);
      emit(LoginSuccessState());
    } catch (err) {
      handleAppError(err);
    }
  }

  _errorLoaded() async {
    await Future.delayed(Duration(seconds: 2));
    throw 'error_message';
  }

  _successLoaded() async {
    await Future.delayed(Duration(seconds: 2));
    return 'Success';
  }
```