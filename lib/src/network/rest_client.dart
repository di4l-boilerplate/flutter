import 'package:dio/dio.dart';
import 'package:flutter_boilerplate/src/common/config.dart';
import 'package:retrofit/retrofit.dart';

part 'rest_client.g.dart';

@RestApi(baseUrl: BASE_URL)
abstract class RestClient {
  factory RestClient(Dio dio, {String baseUrl}) = _RestClient;

//  @POST("api/auth/login-otp")
//  Future<UserAccountResponse> loginWithOTP(
//      @Body() UserAccountRequest userAccountRequest);
}
