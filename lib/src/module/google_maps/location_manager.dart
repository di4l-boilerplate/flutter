/* import 'dart:async';

import 'package:app_settings/app_settings.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:permission_handler/permission_handler.dart';

import 'config.dart';
import 'translation/global_translations.dart';
import 'ui_helper.dart';

class LocationManager with UIHelper {
  Position currentPosition;
  bool permissionGranted = false;
  bool serviceEnabled = false;
  final BuildContext context;
  final geolocator = Geolocator();
  StreamSubscription<Position> _positionStream;
  Function onLocationUpdate;

  LocationManager(this.context);

  initManager() async {
    await _checkService();
    await _checkPermission();
    _initLocationStream();
  }

  _checkPermission() async {
    permissionGranted = await Permission.location.isGranted;
    logger.d("permissionGranted $permissionGranted");
  }

  var _isRequestingPermission = false;
  _requestPermission() async {
    if (_isRequestingPermission) {
      return;
    }
    await _checkPermission();
    if (permissionGranted) {
      _isRequestingPermission = false;
      return;
    }
    _isRequestingPermission = true;
    try {
      logger.d("_isRequestingPermission $_isRequestingPermission");
      permissionGranted =
          await Permission.location.request() == PermissionStatus.granted;
      _isRequestingPermission = false;
    } catch (e) {
      logger.e(e);
    }
  }

  checkAndRequestPermission() async {
    await _checkPermission();
    await _requestPermission();
    _initLocationStream();
  }

  _checkService() async {
    serviceEnabled = await geolocator.isLocationServiceEnabled();
    logger.d("serviceEnabled $serviceEnabled");
  }

  _requestService(BuildContext context) async {
    await _checkService();
    if (!serviceEnabled) {
      showPopUp(context, allTranslations.text("location_is_disabled"),
          onSelect: () async {
        await _checkService();
        if (serviceEnabled) {
          Navigator.of(context).pop();
          _initLocationStream();
        } else {
          AppSettings.openLocationSettings();
        }
      });
      return false;
    }
    return true;
  }

  checkAndRequestService(BuildContext context) async {
    await _checkService();
    _requestService(context);
  }

  validatePermissionAndService() {
    final isValid = permissionGranted && serviceEnabled;
    return isValid;
  }

  _initLocationStream() async {
    if (!validatePermissionAndService()) {
      logger.e("Permission isn't granted or Service is not enabled");
      return;
    }
    if (currentPosition == null) {
      currentPosition = await geolocator.getCurrentPosition();
    }
    if (onLocationUpdate != null && currentPosition != null) {
      onLocationUpdate(currentPosition);
    }
    var locationOptions =
        LocationOptions(accuracy: LocationAccuracy.bestForNavigation);
    _positionStream = geolocator
        .getPositionStream(locationOptions)
        .listen((Position position) {
      // logger.d(position);
      currentPosition = position;
      if (onLocationUpdate != null) {
        onLocationUpdate(position);
      }
    });
  }

  setLocationUpdate(Function onLocationUpdate) async {
    this.onLocationUpdate = onLocationUpdate;
    if (currentPosition != null) {
      onLocationUpdate(currentPosition);
    } else if (permissionGranted && serviceEnabled) {
      currentPosition = await geolocator.getCurrentPosition();
      onLocationUpdate(currentPosition);
    }
  }

  distanceBetween(
      double fromLat, double fromLng, double toLat, double toLng) async {
    final double distance =
        await Geolocator().distanceBetween(fromLat, fromLng, toLat, toLng);
    return distance;
  }
}
 */
