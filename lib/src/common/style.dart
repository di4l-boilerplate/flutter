import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

/// Colors
Color redColor = Color(0xffFE492E);
Color darkred = Color(0xFF9F0000);
Color orangeColor = Color(0xffFF880E);
Color normalTextColor = Color(0xff575757);
Color textColor = Color(0xff000000);
Color lineColor = Color(0xffDBDBDB);
Color blueColor = Color(0xff00558c);
Color darkGreyColor = Color(0xff575757);
Color mainDarkColor = Color(0xFFF8F8F8);
Color blueLightColor = Color(0xff077fce);

/// App colors
Color primaryColor = blueLightColor;
Color secondColor = blueLightColor;
Color darkColor = mainDarkColor;
Color tabIconColor = Colors.grey;
Color tabActiveIconColor = primaryColor;
Color shadow = Color(0xFFD2D3D7);
Color secondDarkColor = Color(0xFF24388a);
Color accentColor = Color(0xFFADC4C8);
Color accentDarkColor = Color(0xFF707070);
Color categoryText = Color(0xFF000000);
Color menuSelect = Color(0xFFFF8C00);

/// common style
const BANNER_ASPECT_RATIO = 0.4;

/// Text styles
Size screenSize(BuildContext context) {
  return MediaQuery.of(context).size;
}

TextStyle inputStyle = TextStyle(fontSize: 14, fontWeight: FontWeight.normal);
TextStyle hintStyle = GoogleFonts.quicksand(
    textStyle: TextStyle(fontSize: 14, fontWeight: FontWeight.normal));
TextStyle titleTextStyle = TextStyle(fontSize: 18, fontWeight: FontWeight.w500);

TextStyle boldTextStyle = TextStyle(fontSize: 18, fontWeight: FontWeight.bold);
TextStyle normalTextStyle =
    TextStyle(fontSize: 18, fontWeight: FontWeight.normal);

TextStyle screenTitleStyle =
    TextStyle(fontSize: 18, fontWeight: FontWeight.bold);

TextStyle boldTitleTextStyle =
    TextStyle(fontSize: 30, fontWeight: FontWeight.bold);

TextStyle largeTitleTextStyle =
    TextStyle(fontSize: 30, fontWeight: FontWeight.normal);

TextStyle largeBoldTextStyle =
    TextStyle(fontSize: 24, fontWeight: FontWeight.bold);

TextStyle buttonTitleStyle =
    TextStyle(fontSize: 17, fontWeight: FontWeight.w700);

TextStyle tabTitleStyle = TextStyle(fontSize: 10, color: Colors.grey);

TextStyle heading35Black = new TextStyle(
    color: Colors.black, fontSize: 35.0, fontWeight: FontWeight.bold);

TextStyle heading30Black = new TextStyle(
    color: Colors.black87, fontSize: 25.0, fontWeight: FontWeight.bold);

TextStyle heading25Black = new TextStyle(
    color: Colors.black, fontSize: 25.0, fontWeight: FontWeight.w900);

TextStyle heading20Black = new TextStyle(
    color: Colors.black, fontSize: 20.0, fontWeight: FontWeight.w900);

TextStyle headingWhite = new TextStyle(
    color: Colors.white, fontSize: 22.0, fontWeight: FontWeight.bold);

TextStyle productTitleStyle = new TextStyle(
    color: Colors.black, fontSize: 12.0, fontWeight: FontWeight.w500);

TextStyle productContentStyle =
    new TextStyle(color: Colors.black, fontSize: 12.0);

TextStyle productPriceStyle = new TextStyle(color: Colors.red, fontSize: 12.0);
