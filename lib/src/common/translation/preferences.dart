import 'dart:async';

import 'package:shared_preferences/shared_preferences.dart';

// Global translation support
const String _storageKey = "Appli_Customer_";
const String _kDefaultLanguage = "en";

Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

Preferences preferences = Preferences();

class Preferences {
  /// ----------------------------------------------------------
  /// Generic routine to fetch a preference
  /// ----------------------------------------------------------
  Future<String> _getApplicationSavedInformation(String name) async {
    final SharedPreferences prefs = await _prefs;

    return prefs.getString(_storageKey + name) ?? '';
  }

  /// ----------------------------------------------------------
  /// Generic routine to saves a preference
  /// ----------------------------------------------------------
  Future<bool> _setApplicationSavedInformation(
      String name, String value) async {
    final SharedPreferences prefs = await _prefs;

    return prefs.setString(_storageKey + name, value);
  }

  /// ----------------------------------------------------------
  /// Method that saves/restores the preferred language
  /// ----------------------------------------------------------
  Future<String> getPreferredLanguage() async {
    return _getApplicationSavedInformation('language');
  }

  setPreferredLanguage(String lang) async {
    return _setApplicationSavedInformation('language', lang);
  }

  static Future<SharedPreferences> _sharePref() async {
    return SharedPreferences.getInstance();
  }

  static Future<String> getLanguages() async {
    var languages = (await _sharePref()).getString('Appli_Customer_language');
    return languages;
  }

  String get defaultLanguage => _kDefaultLanguage;

  // ------------------ SINGLETON -----------------------
  static final Preferences _preferences = Preferences._internal();
  factory Preferences() {
    return _preferences;
  }
  Preferences._internal();
}
