import 'dart:convert';
import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_boilerplate/src/common/storage/app_prefs.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

import 'config.dart';

class NotificationHelper {
  BuildContext context;
  Function(String) onHandleData;

  NotificationHelper(BuildContext context) {
    this.context = context;
    requestPermissionAndListerner();
  }

  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      new FlutterLocalNotificationsPlugin();
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  requestPermissionAndListerner() {
    if (Platform.isIOS) iOS_Permission();
    firebaseCloudMessaging_Listeners();
  }

  firebaseCloudMessaging_Listeners() {
    // if (Platform.isIOS) iOS_Permission();

    var initializationSettingsAndroid =
        new AndroidInitializationSettings('@mipmap/ic_launcher');

    var initializationSettingsIOS = new IOSInitializationSettings(
        onDidReceiveLocalNotification: onDidRecieveLocalNotification);

    var initializationSettings = new InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);

    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: onSelectNotification);

    _firebaseMessaging.getToken().then(
      (token) async {
        print('fcmToken $token');
        AppPref.saveFcmToken(token);
      },
    );

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print('on message $message');
        displayNotification(message);
      },
      onResume: (Map<String, dynamic> message) async {
        var data = message['data'] ?? message;
        print('on resume $data');
        _getdata(json.encode(data));
      },
      onLaunch: (Map<String, dynamic> message) async {
        var data = message['data'] ?? message;
        print('on launch $data');
        await Future.delayed(Duration(seconds: 5));
        _getdata(json.encode(data));
      },
    );
  }

  Future displayNotification(Map<String, dynamic> message) async {
    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
        'channelid', 'flutterfcm', 'your channel description',
        importance: Importance.Max, priority: Priority.High);
    var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
    var platformChannelSpecifics = new NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    if (Platform.isAndroid) {
      await flutterLocalNotificationsPlugin.show(
        0,
        message['notification']['title'],
        message['notification']['body'],
        platformChannelSpecifics,
        payload: json.encode(message['data']),
      );
    } else {
      var data = message['data'] ?? message;
      final title = data['aps']['alert']['title'];
      final body = data['aps']['alert']['body'];
      await flutterLocalNotificationsPlugin.show(
        0,
        title,
        body,
        platformChannelSpecifics,
        payload: json.encode(data),
      );
    }
  }

  Future onSelectNotification(String payload) async {
    if (payload != null) {
      debugPrint('notification payload: ' + payload);
      _getdata(payload);
    }
  }

  _getdata(String payload) {
    logger.d('_getdata: $payload');
    if (onHandleData == null) {
      logger.e('onHandleData not set');
      return;
    }
    onHandleData(payload);
  }

  void iOS_Permission() {
    _firebaseMessaging.requestNotificationPermissions(
        IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
  }

  Future onDidRecieveLocalNotification(
      int id, String title, String body, String payload) async {
    // display a dialog with the notification details, tap ok to go to another page
    showDialog(
      context: context,
      builder: (BuildContext context) => new CupertinoAlertDialog(
        title: new Text(title),
        content: new Text(body),
        actions: [
          CupertinoDialogAction(
            isDefaultAction: true,
            child: new Text('Ok'),
            onPressed: () async {
              Navigator.of(context, rootNavigator: true).pop();
            },
          ),
        ],
      ),
    );
  }
}
