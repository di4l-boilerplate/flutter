import 'dart:async';
import 'dart:io';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_boilerplate/src/screens/network/network_screen.dart';
import 'package:progress_dialog/progress_dialog.dart';

import 'bloc/base_bloc.dart';
import 'config.dart';
import 'translation/global_translations.dart';

class UIHelper {
  var _hasShowDialog = false;
  var _isShowLoading = false;
  BuildContext _context;

  bool isDialogShowing() {
    return _hasShowDialog;
  }

  Future<Uint8List> getBytesFromAsset(
      BuildContext context, String path, int width) async {
    ByteData data = await DefaultAssetBundle.of(context).load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
        targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))
        .buffer
        .asUint8List();
  }

  showPopUp(BuildContext context, String message,
      {String title = '',
      Function onSelect,
      Function onSecondaryAction,
      String primaryActionTitle = 'ok',
      String secondaryActionTitle = '',
      bool barrierDismissible = false,
      bool autoDismiss = true}) async {
    if (_hasShowDialog) {
      return;
    }
    _hasShowDialog = true;
    final isIOS = Platform.isIOS;

    var iOSActions = [
      CupertinoDialogAction(
        isDefaultAction: true,
        child: Text(allTranslations.text(primaryActionTitle)),
        onPressed: () {
          if (autoDismiss) {
            Navigator.of(context).pop();
          }
          if (onSelect != null) {
            onSelect();
          }
        },
      )
    ];

    var androidActions = [
      FlatButton(
        child: Text(allTranslations.text(primaryActionTitle)),
        onPressed: () async {
          if (autoDismiss) {
            Navigator.of(context).pop();
          }

          if (onSelect != null) {
            onSelect();
          }
        },
      )
    ];

    if (secondaryActionTitle.isNotEmpty) {
      iOSActions.add(CupertinoDialogAction(
        isDefaultAction: true,
        child: Text(allTranslations.text(secondaryActionTitle)),
        onPressed: () {
          if (onSecondaryAction != null) {
            onSecondaryAction();
          }
          if (autoDismiss) {
            Navigator.of(context).pop();
          }
        },
      ));

      androidActions.add(FlatButton(
        child: Text(allTranslations.text(secondaryActionTitle)),
        onPressed: () {
          if (onSecondaryAction != null) {
            onSecondaryAction();
          }
          if (autoDismiss) {
            Navigator.of(context).pop();
          }
        },
      ));
    }

    if (isIOS) {
      final alert = new CupertinoAlertDialog(
        title: new Text(title.isEmpty ? appName : title),
        content: new Text(allTranslations.text(message)),
        actions: iOSActions,
      );

      return showCupertinoDialog(
          context: context,
          barrierDismissible: barrierDismissible,
          builder: (context) {
            return alert;
          }).then((_) {
        _hasShowDialog = false;
      });
    } else {
      return showDialog<Null>(
        context: context,
        barrierDismissible: barrierDismissible, // user must tap button!
        builder: (BuildContext context) {
          return WillPopScope(
            onWillPop: () async => false,
            child: new AlertDialog(
              title: new Text(title.isEmpty ? appName : title),
              content: new SingleChildScrollView(
                child: new ListBody(
                  children: <Widget>[
                    new Text(allTranslations.text(message)),
                  ],
                ),
              ),
              actions: androidActions,
            ),
          );
        },
      ).then((_) {
        _hasShowDialog = false;
      });
    }
  }

  ProgressDialog pr;
  Completer<void> refreshCompleter;

  intUI() {
    refreshCompleter = Completer<void>();
  }

  showLoading({@required BuildContext context}) async {
    _context = context;
    // if (pr == null) {
    pr = ProgressDialog(context);
    pr.style(
        message: allTranslations.text("loading"),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 17.0, fontWeight: FontWeight.w600));
    // }
    final val = await pr.show();
    if (!_isShowLoading) {
      return hideLoading();
    }
    return val;
  }

  hideLoading() async {
    var val = false;
    if (pr != null && pr.isShowing()) {
//      logger.d('hideLoading');
      val = await pr.hide();
      return val;
    }

    if (_isShowLoading) {
      return showLoading(context: _context);
    }
    return false;
  }

  hideKeyboard(BuildContext context, {FocusNode focusNode}) {
    if (focusNode != null) {
      if (focusNode.hasPrimaryFocus) {
        focusNode.unfocus();
      } else {
        FocusScope.of(context).unfocus();
      }
    } else {
      // FocusScope.of(context).requestFocus(new FocusNode());
      FocusScope.of(context).unfocus();
    }
  }

  handleCommonState({BuildContext context, BaseState state}) async {
    final internalState = state.internalState;
    if (internalState is LoadingState) {
      _isShowLoading = internalState.isLoading;
      // logger.d("handleCommonState ${internalState.isLoading}");
      if (internalState.isLoading) {
        return await showLoading(context: context);
      } else {
        return await hideLoading();
      }
    }

    if (internalState is NetworkErrorState) {
      globalNavigatorKey.currentState.pushAndRemoveUntil(
          MaterialPageRoute(builder: (_) => NetworkScreen()), (route) => false);
      return false;
    }

    if (internalState is ErrorState) {
      _isShowLoading = false;
      await hideLoading();
      await Future.delayed(Duration(milliseconds: 300));
      return await showPopUp(context, internalState.error);
    }
    return true;
  }

  hideStatusBar() {
    SystemChrome.setEnabledSystemUIOverlays([]);
  }

  showStatusBar() {
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
  }
}
