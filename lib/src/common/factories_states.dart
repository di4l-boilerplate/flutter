import 'package:flutter_boilerplate/src/screens/login/cubit/login_cubit.dart';
import 'package:flutter_boilerplate/src/screens/main/cubit/main_cubit.dart';
import 'package:flutter_boilerplate/src/screens/register/cubit/register_cubit.dart';

import 'bloc/base_bloc.dart';

/// Add factory functions for every Type and every constructor you want to make available to `make`
final factories = <Type, Function>{
  LoginState: (dynamic internalState, LoginState state) =>
      LoginState.fromInternal(internalState, state),
  MainState: (dynamic internalState, MainState state) =>
      MainState.fromInternal(internalState, state),
  RegisterState: (dynamic internalState, RegisterState state) =>
      RegisterState.fromInternal(internalState, state)
};

T make<T extends BaseState>(dynamic internalState, T state) {
  return factories[T](internalState, state);
}
