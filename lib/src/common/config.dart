import 'dart:io';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:logger/logger.dart';

export 'extensions.dart';
export 'resources.dart';
export 'style.dart';

const String appName = 'Flutter Boilerplate';
const String googlePlayIdentifier = "";
const String appStoreIdentifier = "";
const BASE_URL = "https://di4l.vn/";

final GlobalKey<NavigatorState> globalNavigatorKey =
    GlobalKey<NavigatorState>();
final GlobalKey<ScaffoldState> globalScaffoldKey = GlobalKey<ScaffoldState>();
final RouteObserver<PageRoute> routeObserver = RouteObserver<PageRoute>();
Logger logger = Logger();

final currencyFormatter = NumberFormat.currency(locale: 'vi_VN', symbol: 'đ');
final dateFormatter = new DateFormat('yyyy-MM-dd');
final fullDateFormatter = new DateFormat('yyyy-MM-dd hh:mm:ss');

Future<bool> hasNetwork() async {
  try {
    final result = await InternetAddress.lookup('google.com');
    if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
      return true;
    }
  } on SocketException catch (_) {
    return false;
  }
}
