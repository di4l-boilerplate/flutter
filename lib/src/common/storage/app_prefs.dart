import 'package:shared_preferences/shared_preferences.dart';

class AppPref {
  static SharedPreferences _preferences;

  static init() async {
    _preferences = await SharedPreferences.getInstance();
  }

  static String getToken() {
    return _preferences.getString(PrefKey.token) ?? '';
  }

  static Future<bool> setToken(String token) {
    return _preferences.setString(PrefKey.token, token);
  }

  static Future<void> logout() {
    return Future.wait([setToken('')]);
  }

  static saveFcmToken(String token) {
    return _preferences.setString(PrefKey.fcmToken, token);
  }
}

class PrefKey {
  static const String token = 'TOKEN';
  static const String fcmToken = 'FCM_TOKEN';
}
