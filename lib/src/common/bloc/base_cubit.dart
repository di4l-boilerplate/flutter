import 'package:dio/dio.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_boilerplate/src/common/bloc/base_bloc.dart';
import 'package:flutter_boilerplate/src/common/storage/app_prefs.dart';
import 'package:flutter_boilerplate/src/model/base_response.dart';
import 'package:flutter_boilerplate/src/repositories/data_repository.dart';

import '../config.dart';
import '../factories_states.dart';

abstract class BaseCubit<S extends BaseState> extends Cubit<S> {
  BaseCubit(this.dataRepository, S state) : super(state) {
    initData();
  }

  DataRepository dataRepository;

  /// Called when init Cubit
  initData();

  emitLoading(bool isLoading) {
    emit(make<S>(LoadingState(isLoading), state));
  }

  emitError(String error) {
    emit(make<S>(ErrorState(error), state));
  }

  @override
  void onError(Object error, StackTrace stackTrace) {
    handleAppError(error);
  }

  handleAppError(dynamic error) async {
    if (error is String) {
      emit(make<S>(ErrorState(error), state));
    } else if (error is DioError) {
      if (await hasNetwork()) {
        // Check status code
        final token = AppPref.getToken();
        if (error.response?.statusCode == 401 && token.isNotEmpty) {
          // Unauthorized
          // emit(forceLogoutState(state));
          emit(make<S>(ForceLogoutState(), state));
        } else {
          // Common statue, just show error message
          final dioError = _handleDioError(error);
          emit(make<S>(ErrorState(dioError), state));
        }
      } else {
        emit(make<S>(NetworkErrorState(), state));
      }
    } else {
      emit(make<S>(ErrorState('unknow_error'), state));
    }
  }

  String _handleDioError(DioError error) {
    final response = error.response?.data ?? {"error": "server_error"};
    try {
      if (response is Map) {
        final errResponse = BaseResponse.fromJson(response);
        return errResponse.message;
      } else if (response is String) {
        final errResponse = BaseResponse.fromRawJson(response);
        return errResponse.message;
      } else if (error.error is TypeError) {
        final err = error.error as TypeError;
        return err.toString();
      }
    } catch (e) {
      logger.e(e);
    }
    return 'server_error';
  }
}
