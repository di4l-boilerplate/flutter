part of 'base_bloc.dart';

abstract class BaseState {
  dynamic internalState;
}

class ErrorState {
  String error;
  ErrorState(this.error);
}

class LoadingState {
  bool isLoading;
  LoadingState(this.isLoading);
}

class ForceLogoutState {}

class NetworkErrorState {}
