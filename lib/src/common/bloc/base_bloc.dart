// class BaseStateWidget<T extends StatefulWidget> extends State<T>
import 'package:dio/dio.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_boilerplate/src/common/storage/app_prefs.dart';
import 'package:flutter_boilerplate/src/model/base_response.dart';
import 'package:rxdart/rxdart.dart';

import '../config.dart';
import '../factories_states.dart';

part 'base_event.dart';
part 'base_state.dart';

class BaseBloc<E extends BaseEvent, S extends BaseState> extends Bloc<E, S> {
  BaseBloc(S initialState) : super(initialState);

  @override
  S get initialState => throw UnimplementedError();

  @override
  void onError(Object error, StackTrace stackTrace) {
    handleAppError(this, error);
  }

  @override
  Stream<Transition<E, S>> transformEvents(Stream<E> events, transitionFn) {
    return events
        .debounceTime(Duration(milliseconds: 100))
        .switchMap((transitionFn));
  }

  @override
  Stream<S> mapEventToState(E event) async* {
    yield* mapCommonEvents(event);
  }

  handleAppError(Bloc bloc, dynamic error) async {
    if (error is String) {
      bloc.add(InternalErrorEvent(error));
    } else if (error is DioError) {
      if (await hasNetwork()) {
        // Check status code
        final token = AppPref.getToken();
        if (error.response?.statusCode == 401 && token.isNotEmpty) {
          // Unauthorized
          bloc.add(ForceLogoutEvent());
        } else {
          // Common statue, just show error message
          bloc.add(InternalErrorEvent(_handleDioError(error)));
        }
      } else {
        bloc.add(NetworkErrorEvent());
      }
    } else {
      logger.e(error);
      bloc.add(InternalErrorEvent('unknow_error'));
    }
  }

  _handleDioError(DioError error) {
    final response = error.response?.data ?? {"error": "server_error"};
    try {
      if (response is Map) {
        final errResponse = BaseResponse.fromJson(response);
        return errResponse.message;
      } else if (response is String) {
        final errResponse = BaseResponse.fromRawJson(response);
        return errResponse.message;
      } else if (error.error is TypeError) {
        final err = error.error as TypeError;
        return err.toString();
      }
      return;
    } catch (e) {
      logger.e(e);
    }
    return 'server_error';
  }

  Stream<BaseState> mapCommonEvents(BaseEvent event) async* {
    if (event is InternalErrorEvent) {
      yield make<S>(ErrorState(event.error), state);
    } else if (event is ForceLogoutEvent) {
      yield make<S>(ForceLogoutState(), state);
    } else if (event is NetworkErrorEvent) {
      yield make<S>(NetworkErrorState(), state);
    }
  }
}
