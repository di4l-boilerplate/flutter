part of 'base_bloc.dart';

abstract class BaseEvent {}

class InternalErrorEvent extends BaseEvent {
  final String error;
  Map<String, dynamic> values;
  InternalErrorEvent(this.error, {this.values});
}

class ForceLogoutEvent extends BaseEvent {}

class InitDataEvent extends BaseEvent {}

class NetworkErrorEvent extends BaseEvent {}
