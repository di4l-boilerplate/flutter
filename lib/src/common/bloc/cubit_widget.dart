import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_boilerplate/src/common/bloc/base_bloc.dart';
import 'package:flutter_boilerplate/src/common/bloc/base_cubit.dart';
import 'package:flutter_boilerplate/src/common/ui_helper.dart';

abstract class CubitWidget<C extends BaseCubit<S>, S extends BaseState>
    extends StatelessWidget with UIHelper {
  // Default bloc;
  C bloc;

  @override
  Widget build(BuildContext context) {
    bloc = context.bloc<C>();
    WidgetsBinding.instance
        .addPostFrameCallback((_) => afterFirstLayout(context));
    return BlocConsumer<C, S>(
      listener: (context, state) {
        listener(context, state);
      },
      builder: (context, state) {
        return builder(context, state);
      },
    );
  }

  /// afterFirstLayout: This method will be triggerd when UI finished rendered at first time
  void afterFirstLayout(BuildContext context);

  /// Trigger bloc listener events, handle common events as default
  void listener(BuildContext context, S state) {
    handleCommonState(context: context, state: state);
  }

  /// Provider UI builder based on state change
  Widget builder(BuildContext context, S state);
}
