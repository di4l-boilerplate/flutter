import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter_boilerplate/src/common/config.dart';
import 'package:flutter_boilerplate/src/network/rest_client.dart';

class DataRepository implements RestClient {
  final dio = Dio();
  RestClient _client;

  DataRepository() {
    dio.interceptors.add(LogInterceptor(requestBody: true, responseBody: true));
    _client = RestClient(dio, baseUrl: BASE_URL);
    reloadHeaders();
    _initRepo();
  }

  _initRepo() async {}

  Future<void> reloadHeaders() async {
//    dio.options.headers["Authorization"] =
//        "Bearer " + await preferences.getToken();
//    dio.options.headers["Accept-Language"] =
//        await allTranslations.currentLanguage;
  }
}
