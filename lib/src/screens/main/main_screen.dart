import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_boilerplate/src/common/bloc/cubit_widget.dart';
import 'package:flutter_boilerplate/src/repositories/data_repository.dart';
import 'package:flutter_boilerplate/src/screens/main/cubit/main_cubit.dart';

class MainScreen extends CubitWidget<MainCubit, MainState> {
  static provider() {
    return BlocProvider(
      create: (context) =>
          MainCubit(RepositoryProvider.of<DataRepository>(context)),
      child: MainScreen(),
    );
  }

  @override
  Widget builder(BuildContext context, MainState state) {
    return Scaffold(
      body: Center(
        child: Text('Main Screen'),
      ),
    );
  }

  @override
  afterFirstLayout(BuildContext context) {}
}
