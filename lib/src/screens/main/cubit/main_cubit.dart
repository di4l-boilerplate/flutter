import 'package:flutter_boilerplate/src/common/bloc/base_bloc.dart';
import 'package:flutter_boilerplate/src/common/bloc/base_cubit.dart';
import 'package:flutter_boilerplate/src/repositories/data_repository.dart';

part 'main_state.dart';

class MainCubit extends BaseCubit<MainState> {
  MainCubit(DataRepository dataRepository)
      : super(dataRepository, MainInitial());

  @override
  initData() {
    // TODO: implement initData
    throw UnimplementedError();
  }
}
