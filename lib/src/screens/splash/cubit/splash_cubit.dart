import 'package:flutter_boilerplate/src/common/bloc/base_bloc.dart';
import 'package:flutter_boilerplate/src/common/bloc/base_cubit.dart';
import 'package:flutter_boilerplate/src/repositories/data_repository.dart';

part 'splash_state.dart';

class SplashCubit extends BaseCubit<SplashState> {
  SplashCubit(DataRepository dataRepository)
      : super(dataRepository, SplashInitial());

  @override
  initData() async {
    bool _allowLogin = false;
    await Future.delayed(Duration(seconds: 2));
    if (_allowLogin) {
      emit(GoToHomeState());
    } else {
      emit(GoToLoginState());
    }
  }
}
