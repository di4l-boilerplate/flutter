part of 'splash_cubit.dart';

class SplashState extends BaseState {}

class SplashInitial extends SplashState {}

class GoToLoginState extends SplashState {}

class GoToHomeState extends SplashState {}
