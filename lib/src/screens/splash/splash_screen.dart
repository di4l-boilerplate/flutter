import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_boilerplate/routes.dart';
import 'package:flutter_boilerplate/src/common/bloc/cubit_widget.dart';
import 'package:flutter_boilerplate/src/common/config.dart';
import 'package:flutter_boilerplate/src/repositories/data_repository.dart';
import 'package:flutter_boilerplate/src/screens/splash/cubit/splash_cubit.dart';

class SplashScreen extends CubitWidget<SplashCubit, SplashState> {
  static provider() {
    return BlocProvider(
      create: (context) =>
          SplashCubit(RepositoryProvider.of<DataRepository>(context)),
      child: SplashScreen(),
    );
  }

  _builBody() {
    return Container(
      child: Center(child: Image.asset(Images.logo)),
    );
  }

  @override
  Widget builder(BuildContext context, SplashState state) {
    return Scaffold(
      body: _builBody(),
    );
  }

  @override
  listener(BuildContext context, SplashState state) {
    super.listener(context, state);
    if (state is GoToLoginState) {
      Navigator.of(context)
          .pushNamedAndRemoveUntil(AppRoute.loginScreen, (route) => false);
    }

    if (state is GoToHomeState) {
      Navigator.of(context)
          .pushNamedAndRemoveUntil(AppRoute.mainScreen, (route) => false);
    }
  }

  @override
  afterFirstLayout(BuildContext context) {}
}
