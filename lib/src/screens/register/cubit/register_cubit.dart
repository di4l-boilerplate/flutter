import 'package:flutter_boilerplate/src/common/bloc/base_bloc.dart';
import 'package:flutter_boilerplate/src/common/bloc/base_cubit.dart';
import 'package:flutter_boilerplate/src/repositories/data_repository.dart';

part 'register_state.dart';

class RegisterCubit extends BaseCubit<RegisterState> {
  RegisterCubit(DataRepository dataRepository, RegisterState state)
      : super(dataRepository, state);

  @override
  initData() {}
}
