import 'package:flutter/material.dart';
import 'package:flutter_boilerplate/src/common/bloc/cubit_widget.dart';
import 'package:flutter_boilerplate/src/screens/register/cubit/register_cubit.dart';

class RegisterScreen extends CubitWidget<RegisterCubit, RegisterState> {
  @override
  void afterFirstLayout(BuildContext context) {}

  @override
  Widget builder(BuildContext context, RegisterState state) {
    return Scaffold(
      body: Center(child: Text('Register screen')),
    );
  }

  @override
  void listener(BuildContext context, RegisterState state) {
    super.listener(context, state);
  }
}
