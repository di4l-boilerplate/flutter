import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_boilerplate/src/common/bloc/base_bloc.dart';
import 'package:flutter_boilerplate/src/common/bloc/cubit_widget.dart';
import 'package:flutter_boilerplate/src/repositories/data_repository.dart';
import 'package:flutter_boilerplate/src/screens/login/cubit/login_cubit.dart';

class LoginScreen extends CubitWidget<LoginCubit, LoginState> {
  static provider() {
    return BlocProvider(
      create: (context) =>
          LoginCubit(RepositoryProvider.of<DataRepository>(context)),
      child: LoginScreen(),
    );
  }

  @override
  afterFirstLayout(BuildContext context) {
    bloc.demoLoadData();
  }

  @override
  Widget builder(BuildContext context, BaseState state) {
    return Scaffold(
        body: Container(
      child: Center(child: Text("Login Screen")),
    ));
  }

  @override
  void listener(BuildContext context, BaseState state) {
    super.listener(context, state);
    if (state is LoginSuccessState) {
      showPopUp(context, 'Login success');
    }
  }
}
