import 'package:flutter_boilerplate/src/common/bloc/base_bloc.dart';
import 'package:flutter_boilerplate/src/common/bloc/base_cubit.dart';
import 'package:flutter_boilerplate/src/repositories/data_repository.dart';

part 'login_state.dart';

class LoginCubit extends BaseCubit<LoginState> {
  LoginCubit(DataRepository dataRepository)
      : super(dataRepository, LoginInitial());

  @override
  initData() async {}

  demoLoadData() async {
    emitLoading(true);
    await Future.delayed(Duration(seconds: 3));
    try {
      await _errorLoaded();
      // final response = await _successLoaded();
      // print(response);
      emitLoading(false);
      emit(LoginSuccessState());
    } catch (err) {
      handleAppError(err);
    }
  }

  _errorLoaded() async {
    await Future.delayed(Duration(seconds: 2));
    throw 'error_message';
  }

  _successLoaded() async {
    await Future.delayed(Duration(seconds: 2));
    return 'Success';
  }
}
