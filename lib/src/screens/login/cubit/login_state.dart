part of 'login_cubit.dart';

class LoginState extends BaseState {
  String username;

  LoginState(this.username);

  LoginState.fromInternal(dynamic internalState, LoginState state) {
    this.username = state.username;
    this.internalState = internalState;
  }
}

class LoginInitial extends LoginState {
  LoginInitial() : super('');
}

class LoginSuccessState extends LoginState {
  LoginSuccessState() : super('');
}
