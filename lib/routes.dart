import 'package:flutter/material.dart';
import 'package:flutter_boilerplate/src/screens/login/login_screen.dart';

import 'src/screens/main/main_screen.dart';
import 'src/screens/splash/splash_screen.dart';

class PageViewTransition<T> extends MaterialPageRoute<T> {
  PageViewTransition({WidgetBuilder builder, RouteSettings settings})
      : super(builder: builder, settings: settings);

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    if (animation.status == AnimationStatus.reverse)
      return super
          .buildTransitions(context, animation, secondaryAnimation, child);
    return FadeTransition(opacity: animation, child: child);
  }
}

class AppRoute {
  static const String splashScreen = '/splashScreen';
  static const String mainScreen = '/mainScreen';
  static const String loginScreen = '/loginScreen';

  static Route<dynamic> generateRoute(RouteSettings settings) {
    final mapData =
        settings.arguments as Map<String, dynamic> ?? Map<String, dynamic>();
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(
            builder: (context) => SplashScreen.provider(), settings: settings);
      case splashScreen:
        return MaterialPageRoute(
            builder: (context) => SplashScreen.provider(), settings: settings);
      case loginScreen:
        return MaterialPageRoute(
            builder: (context) => LoginScreen.provider(), settings: settings);
      case mainScreen:
        return MaterialPageRoute(
            builder: (context) => MainScreen.provider(), settings: settings);
      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
                  body: Center(
                      child: Text('No route defined for ${settings.name}')),
                ));
    }
  }
}
