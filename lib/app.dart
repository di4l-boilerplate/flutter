import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_boilerplate/src/common/config.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'routes.dart';
import 'src/common/notification_helper.dart';
import 'src/common/ui_helper.dart';
import 'src/repositories/data_repository.dart';
import 'src/screens/splash/splash_screen.dart';
import 'src/util/device_helper.dart';

class Application extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _ApplicationState createState() => _ApplicationState();
}

class _ApplicationState extends State<Application> with UIHelper {
  DataRepository _dataRepository;
  NotificationHelper _notificationHelper;

  @override
  void initState() {
    _dataRepository = DataRepository();
    super.initState();
    DeviceHelper.instance.setContext(context);
    _notificationHelper = NotificationHelper(context);
    _notificationHelper.onHandleData = _handleClickNotification;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: MultiRepositoryProvider(
        providers: [
          RepositoryProvider<DataRepository>(
            create: (context) => _dataRepository,
          ),
        ],
        child: RefreshConfiguration(
          headerBuilder: () => MaterialClassicHeader(),
          footerBuilder: () => ClassicFooter(),
          child: MaterialApp(
            title: appName,
            navigatorKey: globalNavigatorKey,
            key: globalScaffoldKey,
            color: Colors.white,
            theme: _buildThemeData(),
            localizationsDelegates: [
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
              const FallbackCupertinoLocalisationsDelegate(),
            ],
            supportedLocales: [
              const Locale('en', 'US'), // English
              const Locale('vi', ''), // Vietnamese
            ],
            debugShowCheckedModeBanner: false,
            onGenerateRoute: AppRoute.generateRoute,
            home: SplashScreen.provider(),
            navigatorObservers: [routeObserver],
          ),
        ),
      ),
    );
  }

  ThemeData _buildThemeData() {
    return ThemeData(
        primaryColor: primaryColor,
        primaryTextTheme:
            GoogleFonts.quicksandTextTheme(Theme.of(context).textTheme),
        textTheme: GoogleFonts.quicksandTextTheme(Theme.of(context).textTheme));
  }

  _handleClickNotification(String payloadData) {
    logger.i(payloadData);
  }
}

class FallbackCupertinoLocalisationsDelegate
    extends LocalizationsDelegate<CupertinoLocalizations> {
  const FallbackCupertinoLocalisationsDelegate();

  @override
  bool isSupported(Locale locale) => true;

  @override
  Future<CupertinoLocalizations> load(Locale locale) =>
      DefaultCupertinoLocalizations.load(locale);

  @override
  bool shouldReload(FallbackCupertinoLocalisationsDelegate old) => false;
}
